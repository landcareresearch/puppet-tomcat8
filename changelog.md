## 2018-02-27 Release 0.2.15
Fixed minor issue with ppa exec

### Changed
  - The ppa didn't have a check to see if it already existed.  Added the check
    to prevent multiple runs.

## 2018-02-27 Release 0.2.14
Added ppa for java 8 on trusty

### Changed
  - Ubuntu 14.04 (trusty) does not have openjdk 8 in the repositories.
    There is however a ppa.  This module detects for Trusty and if so
    uses the PPA.  This module does NOT enable the ppa for other distributions.
  - Updated readme with additional parameters.

## 2018-02-02 Release 0.2.13
## 2018-02-02 Release 0.2.12
Added a parameter

### Changed
  - Added max_post_size parameter

## 2018-01-29 Release 0.2.11
Fixes for underused classes

### Changed
  - several subclasses had fixes that were not used in production but will be with snowflake

## 2018-01-23 Release 0.2.10
Removed Oracle Java

### Changed
  - Removed oracle java
  - Use openjdk

## 2017-09-28 Release 0.2.9
Added subscribe to reload tomcat

### Changed
  - Added a subscribe to reload tomcat on config changes

## 2017-09-28 Release 0.2.8
Added parameters for jmx monitoring

### Changed
  - Added parameters to enable jmx monitoring.

## 2017-09-15 Release 0.2.7
Update comment for changes in configuration.

### Changed
  - A command is issued to ensure configuraiton changes are instanitated.

## 2017-06-13 Release 0.2.6
Fixed bug in allow_linking

### Changed
  - Two resource elements were were being written to the contenxt.xml file.
    This fix combines the resource elements into 1 element.

## 2017-05-12 Release 0.2.5
Fixed bug in context.xml

### Changed
  - '\n' was being written to context.xml instead of return character.

## 2017-05-12 Release 0.2.4
Fixed logging bug

### Changed
  - Tomcat 8 fills up log files and doesn't rotate them.  This can easily cause out of disk space issues.
    logrotate has been added.
  - Changed the log file handlers so no rotation happens by tomcat.  Rotation is handled by logrotate.

## 2017-04-14 Release 0.2.3
### Summary
Added a new parameter for controlling the cache.

### Changed
  - Added the cache_max_size parameter.

## 2017-04-13 Release 0.2.2
### Summary
Updated webapps dir handling

### Changed
  - Copies the webapps dir if the dir was changed.

## 2017-04-13 Release 0.2.1
### Summary
Added webapps dir parameter

### Changed
  - Added a webapps directory parameter.

## 2017-04-12 Release 0.2.0
### Summary
Puppet 4

### Changed
  - Added data types for parameters
  - Added a new parameter for making a dependency an option.

## 2016-09-12 Release 0.1.4

### Summary
Changed download URL.

### Changed
  - Changed the Download URL to archive.

## 2016-09-07 Release 0.1.3

### Summary
Added symlink support

### Changed
  - Added symlink support.  See http://stackoverflow.com/questions/22240776/symlinking-tomcat-8-directory-resources

## 2016-09-06 Release 0.1.2

### Summary
Completed Implementation

### Changed
  - Fixed manager sub class so installs manager properly.
  - Added compatibility for Ubuntu 14.04 and 16.04.
  - Fixed app defined type so that it properly deploys webapps.
  - Fixed various requirement statements which used the old package method
    of installing tomcat instead of using the new method of installing
    via tarball.

## 2016-08-08 Release 0.1.1
## 2016-08-08 Release 0.1.0

### Summary
Initial Release

