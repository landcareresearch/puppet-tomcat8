# Tomcat8 puppet module

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/tomcat8.svg)](https://forge.puppetlabs.com/landcaresearch/tomcat8)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_Puppet_3_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_Puppet_3_Build&guest=1)

## About

This module installs, configures, and manages Apache Tomcat8.
Note, uses the openjdk's jvm.

## Requirements
Note, the puppet packages below should automatically be installed if the puppet module command is used.

 * Ubuntu Operating System.
 * Depedencies listed in the Modulefile.

## Configuration

There is one class that needs to be declared.  The class installs and manages a single instance of tomcat8.  The tomcat8 class is a paramaterized class with 1 optional paramater.
There is one defined type called tomcat8::app.  The tomcat8::app defines a webapp to be installed into the typical webapp directory of tomcat.  

### Tomcat8 Optional Parameter

The parameters listed in this section can optionally be configured.

#### `memory`
Sets the memory allocated for tomcat.
Default: '128m'

#### `additional_java_options`
A string that adds to the java options in /etc/default/tomcat8.
Default: undef

#### `port`
The default port for http connection.  
Default: '8080'

#### `redirect_port`
To specify if tomcat should add the redirect port attribute.  
Default: undef

#### `enable_tomcat_proxy`
Set to enable or disable the tomcat proxy.  
Default: false

#### `tomcat_proxy_name`
The name of the proxy.  
Default: undef

#### `tomcat_proxy_port`
The port that was proxied from.  
Default: undef

#### `tomcat_proxy_scheme`
The scheme used for the proxy such as http or https.  
Default: 'https'

#### `java_version`
The openjdk version of java to use.  Note, if java 7 is used on Ubuntu 14.04, a ppa will be used to install openjdk.
Valid values:
   * 7
   * 8
   Default: '8'

#### `allow_linking`
True if symlinks are allowed and false otherwise.  
Default: false

#### [`cache_max_size`](http://tomcat.apache.org/tomcat-8.0-doc/config/resources.html)
The maximum size of the static resource cache in kilobytes.  
Default: '10240'

#### [`max_post_size`]
The maximum size in bytes of the POST which will be handled by the container FORM URL parameter parsing.  
Default: 2097152

#### `webapps_dir`
The directory where webapps will be installed.  If left as undef, the directory webapps will be installed is <tomcat_install>/webapps.  
Default: undef

#### `set_jmx_params`
Enable jmx monitoring.  
Default: false

#### `jmx_port`
The port for jmx to serve.  
Default: 3000

#### `context_content`
Additional content to be added to the context.xml file.  
Default: undef

## Usage

This section shows example uses of the tomcat6 module.

### Example 1
This example demonstrates the most basic usage of the tomcat8 module.
Deploys tomcat8 with the default configuration.

	class {'tomcat8': }


### Deploying a tomcat web app.
*defined type*: tomcat8::app
Web apps are considered resources (ie defined types).  This allows
for any number of web apps to be deployed on this service.

The web app Creates a new app with the name of the app is the title of this defined type. An app is deployed through a war file.
This defiend type also handles upgrades if the name of the app title remains the same and the war filename changes.

NOTE, the wars are downloaded to the $tomcat::$tomcat_download_dir and are unzipped to the $tomcat::webapps directory
#### Paramaters
##### `war_url`
The url that points to the war to download.

##### `web_user`
The user name of the url to download.
Default: undef

##### `web_password`
The user's password to download the war and jars.
Default: undef


#### Example 1
```
 tomcat8::app { 'app':
   $war_url = 'http://localhost/files/app.0.1.war'
 }
}
```

### Installing external jars for web apps
*defined type*: tomcat8::jar
Its sometimes necessary to install external depandancies for a tomcat application.  For instance, adding a jdbc driver.

#### Parameters
##### `app_name`
The name of the app to install this lib to.


##### `url`
An additional jar to install to the app (usually a jdbc driver).

##### `web_user`
The user name of the url to download.
Default: undef

##### `web_password`
The user's password to download the war and jars.
Default: undef

### Installing shared libs
*defined type*: tomcat8::shared_libs
Installing system wide deplandancies making libraries or license files available to all deployed webapps can be achived through this resource.

This resource downloads and installs a file into tomcat's lib directory.

#### Parameters
##### `url`
An file to download and install to the tomcat's lib directory.

##### `filename`
If the name of the file is to be different than the filename from the url, the name of the file can be set.
Default: undef

##### `web_user`
The user name of the url to download.
Default: undef

##### `web_password`
The user's password to download the file.
Default: undef

### Installing resources
*defined type*: tomcat8::resource
Resources are xml elements defined in the context.xml
Note, the title will be used as the comment before the resource.

#### Parameters
##### `context`
A string that defines the resource.  Must be a complete and valid
xml element as defined in the tomcat documentation for Resource Definitions.
An example of the string contents are listed below.

```
<Resource name="jdbc/postgres" auth="Container"
          type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
          url="jdbc:postgresql://127.0.0.1:5432/mydb"
          username="myuser" password="mypasswd" maxActive="20" maxIdle="10"
maxWait="-1"/>
```

## Limitations

Only works with debian based OS's.
