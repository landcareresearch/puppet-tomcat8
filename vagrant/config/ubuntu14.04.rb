module MyVars
  OS     = "ubuntu/trusty64"
  OS_URL = "https://atlas.hashicorp.com/ubuntu/boxes/trusty64"
  #OS     = "puppetlabs/ubuntu-14.04-64-puppet"
  #OS_URL = "https://atlas.hashicorp.com/puppetlabs/boxes/ubuntu-14.04-64-puppet"
  PUPPET = "scripts/upgrade-puppet.sh"
end
