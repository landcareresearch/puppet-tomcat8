# == Class: tomcat8::manager
#
# Defines the tomcat 7 application manager.
# Note, does not require any dependancies as its handled in this class.
#
# === Parameters
# [*password*]
#   The password for the manager account.
#
# [*user*]
#   The name of the user to access the account.
#   Default: admin
#
# [*allow*]
#   The regex for allowing access to the manager.
#   The default is to only allow access from the localhost.
#   Default: '127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1'
#
# === Variables
#
# === Example
#
class tomcat8::manager(
  $password,
  $user  = 'admin',
  $allow = '127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1|192\.\d+\.\d+\.\d+',
)
{

  anchor{'tomcat8::manager::begin':
    require => Deploy_zip::Tar['tomcat8'],
  }

  file{"${tomcat8::tomcat_webapps}/manager/META-INF/context.xml":
    ensure  => file,
    content => template('tomcat8/manager_context.xml.erb'),
    require => Anchor['tomcat8::manager::begin'],
  }

  file {"${tomcat8::tomcat_conf}/tomcat-users.xml":
    ensure  => file,
    group   => 'tomcat',
    content => "<tomcat-users>\n\
 <user username=\"${user}\" password=\"${password}\"\
 roles=\"manager-gui,admin-gui\"/>\n\
</tomcat-users>\n",
    notify  => Service['tomcat8'],
    require => File["${tomcat8::tomcat_webapps}/manager/META-INF/context.xml"],
  }

  anchor{'tomcat8::manager::end':
    require => File["${tomcat8::tomcat_conf}/tomcat-users.xml"],
  }
}
