# == Class: tomcat8::params
#
# Contains the default parameters for tomcat.
#
# === Variables
#
#
class tomcat8::params{

  case $::lsbdistrelease {
    '14.04': {
      $start_script_path    = '/etc/init/tomcat8.conf'
      $start_script_content = 'tomcat8/tomcat.conf.erb'
    }
    default: {
      $start_script_path    = '/etc/systemd/system/tomcat8.service'
      $start_script_content = 'tomcat8/tomcat.service.erb'
    }
  }

  $required_packages = ['software-properties-common']
}