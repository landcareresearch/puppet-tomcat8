# == Defined Type: tomcat8::jar
#
# Downloads and installs a jar for a tomcat8::app.
#
# [*app_name*]
#   The name of the app to install this lib to.
#
# [*url*]
#   An additional jar to install to the app (usually a jdbc driver).
#
# [*web_user*]
#   The user name of the url to download.
#   Default: undef
#
# [*web_password*]
#   The user's password to download the war and jars.
#   Default: undef
#
define tomcat8::jar (
  $app_name,
  $url,
  $web_user     = undef,
  $web_password = undef,
){
    # variables
    $lib_name_array = split($url,'/')
    $lib_name       = $lib_name_array[-1]
    wget::fetch{"${title}_download_jar":
      source      => $url,
      destination =>
      "${tomcat8::tomcat_webapps}/${app_name}/WEB-INF/lib/${lib_name}",
      user        => $web_user,
      password    => $web_password,
      timeout     => 0,
      verbose     => false,
      #cache_dir   => $tomcat8::wget_cache,
    }
}
