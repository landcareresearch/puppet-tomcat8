# == Define: tomcat8::resource
#
# Defines a tomcat resource that will be added.
# The title will be used as the comment before the resource.
#
# === Parameters
#
# [*context*]
#   A string that defines the resource.  Must be a complete and valid
#   xml element as defined in the tomcat documentation for Resource Definitions.
#
# === Variables
#
# === Example
#
define tomcat8::resource (
  $context,
){

  concat::fragment{"context_${title}":
    target  => $tomcat8::tomcat_context,
    content => "<!-- ${title} -->\n${context}\n",
    order   => '05',
  }
}
