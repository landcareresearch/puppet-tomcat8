# == Define: tomcat8::app
#
# Creates a new app with the name of the app is the title of this defined type.
# An app is deployed through a war file.
# Handles upgrades if the name of the app title remains the same and the war
# filename changes.
#
# NOTE, the wars are downloaded to the $tomcat::$tomcat_download_dir
# and are unzipped to the $tomcat::webapps directory
#
# === Parameters
#
# [*war_url*]
#   The url that points to the war to download.
#
# [*web_user*]
#   The user name of the url to download.
#
# [*web_password*]
#   The user's password to download the war and jars.
#
# === Variables
#
# [*fetch_name*] Used as the name of the task for downloading the war file.
#
# === Upgrade Example
#
# ==== On First Puppet run
# tomcat8::app { 'appname'
#   $war_url = 'http://localhost/files/app.0.1.war'
# }
# this will
# * download war
# * unzip the war into the webapps directory
# * notify tomcat
#
# Now, the user wants to deploy app.0.2.war
#
# ==== On Second Puppet run
# the code looks like this
# tomcat8::app { 'appname':
#   $war_url = 'http://localhost/files/app.0.2.war'
# }
# this module will
# * delete webapps/app directory
# * download the new war
# * unzip the new war to app
# * notify tomcat
#
define tomcat8::app (
  $war_url,
  $web_user          = undef,
  $web_password      = undef,
){

  if ! defined(Class['tomcat8']) {
    fail("You must include the tomcat8 base class before using any tomcat8\
 defined resources")
  }

  $pre_fetch      = "tomcat8_app_prefetch_${title}"
  $fetch_name     = "download_${title}"
  $task_unzip     = "task_unzip_${title}"
  $filename_array = split($war_url,'/')
  $filename       = $filename_array[-1]

  # attempt to delete
  exec { $pre_fetch:
    command => "/bin/rm -rf ${tomcat8::tomcat_webapps}/${title}",
    ## 2 conditions must be met for this to run
    onlyif  => [
      # the app must not have already been downloaded
      "/usr/bin/test ! -f ${tomcat8::tomcat_download_dir}/${filename}",
      # && the app directory must exist
      "/usr/bin/test -d ${tomcat8::tomcat_webapps}/${title}"
    ]
  }

  # make sure the webapps dir is available
  if ! defined(File[$tomcat8::tomcat_webapps]) {
    file { $tomcat8::tomcat_webapps:
      ensure  => directory,
      require => Deploy_zip::Tar['tomcat8'],
    }
  }
  # make sure the download dir is available
  if ! defined(File[$tomcat8::tomcat_download_dir]) {
    file { $tomcat8::tomcat_download_dir:
      ensure  => directory,
      require => Deploy_zip::Tar['tomcat8'],
    }
  }

  include wget
  # download the war
  wget::fetch {$fetch_name:
    source      => $war_url,
    destination => "${tomcat8::tomcat_download_dir}/${filename}",
    user        => $web_user,
    password    => $web_password,
    timeout     => 0,
    verbose     => false,
    #cache_dir   => $tomcat8::wget_cache,
    notify      => Service['tomcat8'],
    require     => [ File[$tomcat8::tomcat_webapps,
$tomcat8::tomcat_download_dir],
Exec[$pre_fetch]],
  }

  # packages for unzipping
  include stdlib
  ensure_packages(['unzip'])

  #unzip war
  exec { $task_unzip:
    command => "/usr/bin/unzip -d ${tomcat8::tomcat_webapps}/${title}\
 ${tomcat8::tomcat_download_dir}/${filename}",
    # only unzip if the directory doesn't exists
    onlyif  => "/usr/bin/test ! -d ${tomcat8::tomcat_webapps}/${title}",
    require => [Package['unzip'],
                Wget::Fetch[$fetch_name]],
  }

  file{ ["${tomcat8::tomcat_webapps}/${title}",
          "${tomcat8::tomcat_webapps}/${title}/WEB-INF",
          "${tomcat8::tomcat_webapps}/${title}/WEB-INF/lib"]:
    ensure  => directory,
    require => Exec[$task_unzip],
  }
}
