## == Class: tomcat8
#
# Installs tomcat8 with the defuault settings
# and ensures that its running
#
# === Parameters
#
# TODO - need to add systemctl daemon-reload  & restart tomcat to realize changes
#        for memory!!!!
#
# [*memory*]
#   The maximum amount of memory to be used by tomcat.
#    Default is '128m'
#
# [*additional_java_options*]
#  A string that adds to the java options in /etc/default/tomcat8.
#
# [*port*]
#   The default port for http connection.
#   Default: '8080'
#
# [*redirect_port*]
#   To specify if tomcat should add the redirect port attribute.
#   Default: undef
#
# [*enable_tomcat_proxy*]
#   Set to enable or disable the tomcat proxy.
#   Default: false
#
# [*tomcat_proxy_name*]
#   The name of the proxy.
#   Default: undef
#
# [*tomcat_proxy_port*]
#   The port that was proxied from.
#   Default: undef
#
# [*tomcat_proxy_scheme*]
#   The scheme used for the proxy such as http or https.
#   Defulat: 'https'
#
# [*java_version*]
#   The version of java to use.
#   Valid values:
#   * 8
#   Default: '8'
#
# [*allow_linking*]
#   True if symlinks are allowed and false otherwise.
#   Default: false
#
# [*cache_max_size*]
#   The maximum size of the static resource cache in kilobytes.
#   http://tomcat.apache.org/tomcat-8.0-doc/config/resources.html
#   Default: '10240'
#
# [*max_post_size*]
# The maximum size in bytes of the POST which will be handled by the
# container FORM URL parameter parsing.
# Default: 2097152
#
# [*webapps_dir*]
#   The directory where webapps will be installed.
#   If left as undef, the directory webapps will be installed is
#   <tomcat_install>/webapps
#   Default: undef
#
# [*set_jmx_params*]
#   Enable jmx monitoring.
#   Default: false
#
# [*jmx_port*]
#   The port for jmx to serve.
#   Default: 3000
#
# [*context_content*]
#   Additional content to be added to the context.xml file.
#   Default: undef
#
# === Variables
#
# [*tomcat_root*]
#   The root directory where tomcat is installed.
#
# [*tomcat_webapps*]
#   The directory where webapps will be installed.
#
# [*tomcat_download_dir*]
#   The directory where tomcat webapps will be downloaded to first before
#   extraction.
#
# [*tomcat_context*]
#   The path to the context.xml file for tomcat resources.
#
# [*tomcat_lib*]
#   A directory for installing libs
#
# [*tomcat_home*]
#   The home directory for the tomcat8 users.
#
# [*tomcat_java_fix*]
#   The directories necessary to prevent tomcat from presenting
#   errors regarding not being able to access lock files.
#
# [*wget_cache*]
#   The directory to store a cache for wget.
#
# [*connector_head*]
#   The string for the default connector.
#
# [*connector_tail*]
#   The string for the end of the connector used to determine if a proxy
#   should be specified or not.
#
# [*connector_context*]
#   The concat of connector_head and connector_tail.
#
# [*is_require_packages*]
#   If true, sets a dependancy on the params::required_packages
#   Default: true
#
class tomcat8 (
  $memory                           = '128m',
  $additional_java_options          = undef,
  String  $port                     = '8080',
  $redirect_port                    = undef,
  Boolean $enable_tomcat_proxy      = false,
  $tomcat_proxy_name                = undef,
  $tomcat_proxy_port                = undef,
  String  $tomcat_proxy_scheme      = 'https',
  String  $tomcat_version           = '8.5.5',
  String  $java_version             = '8',
  Boolean $allow_linking            = false,
  String  $cache_max_size           = '10240',
  String  $max_post_size            = '2097152',
  Boolean $is_require_packages      = false,
  Boolean $set_jmx_params           = false,
  Integer $jmx_port                 = 3000,
  Optional[String] $webapps_dir     = undef,
  Optional[String] $context_content = undef
) inherits tomcat8::params{

  anchor{'tomcat8::begin':}

  $tomcat_root         = '/opt/tomcat8'
  $tomcat_conf         = "${tomcat_root}/conf"
  $tomcat_log          = "${tomcat_root}/logs"
  $tomcat_download_dir = '/opt/tomcat8_download'
  $tomcat_context      = "${tomcat_conf}/context.xml"
  $tomcat_server       = "${tomcat_conf}/server.xml"
  $tomcat_log_prop     = "${tomcat_conf}/logging.properties"
  $tomcat_home         = $tomcat_root
  $tomcat_lib          = "${tomcat_home}/lib"
  $tomcat_java_fix     = ["${tomcat_home}/.java",
                          "${tomcat_home}/.java/userPrefs"]
  $wget_cache          = '/var/cache/wget'

  if $webapps_dir {
    $tomcat_webapps = $webapps_dir
  }else{
    $tomcat_webapps = "${tomcat_root}/webapps"
  }

  $tomcat_url = "http://archive.apache.org/dist/tomcat/tomcat-8/v\
${tomcat_version}/bin/apache-tomcat-${tomcat_version}.tar.gz"

  if $set_jmx_params {
    $jmx_params = " -Dcom.sun.management.jmxremote\
 -Dcom.sun.management.jmxremote.port=${jmx_port}\
 -Dcom.sun.management.jmxremote.ssl=false\
 -Dcom.sun.management.jmxremote.authenticate=false"
  }else{
    $jmx_params=''
  }

  if $additional_java_options != undef {
    $java_options = "-Djava.awt.headless=true\
 -Djava.security.egd=file:/dev/./urandom\
 ${additional_java_options}"

    $catalina_options = "-Xmx${memory} -Xms${memory}\
 -server -XX:+UseConcMarkSweepGC ${jmx_params} "
  } else {
    $java_options = "-Djava.awt.headless=true\
 -Djava.security.egd=file:/dev/./urandom"

    $catalina_options = "-Xmx${memory} -Xms${memory}\
 -server -XX:+UseConcMarkSweepGC ${jmx_params} "
  }

  if $redirect_port {
    $redirect_port_string = "redirectPort=\"${redirect_port}\""
  }else{
    $redirect_port_string = ''
  }

  $connector_head = "
      <Connector port=\"${port}\" protocol=\"HTTP/1.1\"
               connectionTimeout=\"20000\"
               URIEncoding=\"UTF-8\"
               MaxPostSize=\"${max_post_size}\"
               ${redirect_port}"
  if $enable_tomcat_proxy {
    $connector_tail = "scheme=\"${tomcat_proxy_scheme}\" \
proxyName=\"${tomcat_proxy_name}\" proxyPort=\"${tomcat_proxy_port}\" />\n\n"
  }else {
    $connector_tail = "/>\n\n"
  }
  $connector_context = "${connector_head} ${connector_tail}"

  ### === Install JAVA === ###

  ensure_packages($tomcat8::params::required_packages)

  if $is_require_packages {
    $java_require = [Anchor['tomcat8::begin'],
                      Package[$tomcat8::params::required_packages]]
  }else{
    $java_require = Anchor['tomcat8::begin']
  }

  notice("LSB = ${::lsbdistrelease}")
  if $::lsbdistrelease == '14.04' and $java_version == '8' {
    # add the ppa
    exec{'add_openjdk_ppa':
      command => '/usr/bin/add-apt-repository ppa:openjdk-r/ppa',
      creates => '/etc/apt/sources.list.d/openjdk-r-ppa-trusty.list',
      require => $java_require,
    }
    exec{'openjdk_apt_get_update':
      command     => '/usr/bin/apt-get update',
      refreshonly => true,
      subscribe   => Exec['add_openjdk_ppa'],
      before      => Class['openjdk'],
    }
  }

  class{'openjdk':
    version => $java_version,
    require => $java_require,
  }
  #$java_home = $openjdk::java_home
  $java_home = "/usr/lib/jvm/java-${java_version}-openjdk-${::architecture}"

  ## configure the cache directory if it doesn't exist
  if ! defined(File[$wget_cache]) {
    file{$wget_cache:
      ensure  => directory,
      require => Class['openjdk'],
      before  => Deploy_zip::Tar['tomcat8'],
    }
  }
  ### === Install Tomcat === ###

  user{'tomcat':
    home => $tomcat_home,
  }

  include deploy_zip
  deploy_zip::tar {'tomcat8':
    deployment_dir        => '/opt',
    url                   => $tomcat_url,
    manage_deployment_dir => false,
    owner                 => 'tomcat',
    group                 => 'tomcat',
    require               => User['tomcat'],
  }

  # if webapps dir is different, need to change it
  if $webapps_dir {
    exec{'copy_webapps':
      command     => "/bin/cp -r ${tomcat_root}/webapps/* ${tomcat_webapps}/.",
      refreshonly => true,
      subscribe   => Deploy_zip::Tar['tomcat8'],
    }
  }

  # configure start script
  file{$tomcat8::params::start_script_path:
    ensure  => file,
    mode    => '0755',
    content => template($tomcat8::params::start_script_content),
  }

  # used since the tomcat8::app also needs this directory to exists.
  if ! defined(File[$tomcat8::tomcat_webapps]) {
    file { $tomcat8::tomcat_webapps:
      ensure  => directory,
      require => File[$tomcat8::params::start_script_path],
      before  => File['/etc/default/tomcat8'],
    }
  }
  file {'/etc/default/tomcat8':
    ensure  => present,
    content => template('tomcat8/tomcat8.erb'),
    require => File[$tomcat8::params::start_script_path],
  }
  concat { $tomcat_context:
    ensure  => present,
    require => File['/etc/default/tomcat8'],
  }
  concat::fragment{'context_head':
    target  => $tomcat_context,
    content => "<?xml version='1.0' encoding='utf-8'?>\n\
<!-- The contents of this file will be loaded for each web application -->\n\
<!-- AUTO GENERATED: DO NOT MANUALLY EDIT -->\n\
<Context>\n",
    order   => '01',
  }

  # create the Resource element
  $cache_resource =
  "cachingAllowed=\"true\" cacheMaxSize=\"${cache_max_size}\""

  if $allow_linking {
    $linking_resource = 'allowLinking="true"'
  }else{
    $linking_resource = 'allowLinking="false"'
  }
  $resources = "${linking_resource} ${cache_resource}"

  concat::fragment{'cache_max_size':
    target  => $tomcat_context,
    content => "    <Resources ${resources} />\n",
    order   => '04',
  }

  if $context_content {
    concat::fragment{'context_content':
      target  => $tomcat_context,
      content => $context_content,
      order   => '05',
    }
  }

  concat::fragment{'context_tail':
    target  => $tomcat_context,
    content => '</Context>',
    order   => '99',
  }

  # manage the server.xml file
  concat {$tomcat_server:
    ensure  => present,
    require => Concat[$tomcat_context],
  }
  concat::fragment{'server_head':
    target => $tomcat_server,
    source => 'puppet:///modules/tomcat8/server_head.xml',
    order  => '01',
  }
  concat::fragment{'server_tail':
    target  => $tomcat_server,
    content => template('tomcat8/server_tail.xml.erb'),
    order   => '99',
  }
  # add default connector
  concat::fragment{'server_default_connector':
    target  => $tomcat_server,
    content => $connector_context,
    order   => '02',
  }

  file {$tomcat_log_prop:
    ensure  => file,
    content => template('tomcat8/logging.properties.erb'),
    require => Concat[$tomcat_server],
  }

  file {$tomcat_lib:
    ensure  => directory,
    #require => Concat[$tomcat_server],
    require => File[$tomcat_log_prop],
  }

  # fix for problem with tomcat logging every 30 seconds
  # can't get lock file
  file{$tomcat_java_fix:
    ensure  => directory,
    owner   => 'tomcat',
    group   => 'tomcat',
    require => File[$tomcat_context],
  }

  # restart the daemon to have the memory changes takes effect.
  exec{'daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    require     => File[$tomcat_java_fix],
    subscribe   => File['/etc/default/tomcat8',$tomcat_context,$tomcat_lib],
  }

  service {'tomcat8':
    ensure    => running,
    require   => [File[$tomcat_java_fix], Exec['daemon-reload']],
    subscribe => File['/etc/default/tomcat8',$tomcat_context,$tomcat_lib,
                    $tomcat8::params::start_script_path],
  }

  if !defined(::logrotate){
    class { '::logrotate':
      ensure => 'latest',
      config => {
        dateext      => true,
        compress     => true,
        rotate       => 4,
        rotate_every => 'week',
        su_group     => 'syslog',
        require      => Anchor['profile::logrotate::begin'],
        before       => Anchor['profile::logrotate::end'],
      }
    }
  }

  # add log rotation with logrotate instead of built in java one
  logrotate::rule{'tomcat8.catalina.out':
    path    => "${tomcat_log}/catalina.out",
    require => Service['tomcat8'],
  }

  logrotate::rule{'tomcat8.catalina.log':
    path    => "${tomcat_log}/catalina.log",
    require => Service['tomcat8'],
  }

  logrotate::rule{'tomcat8.access':
    path    => "${tomcat_log}/access_log",
    require => Service['tomcat8'],
  }

  anchor{'tomcat8::end':
    require => [Service['tomcat8'],
                Logrotate::Rule['tomcat8.catalina.out',
                'tomcat8.catalina.log',
                'tomcat8.access']],
  }
}
