# == Class: tomcat8::security
#
# Manages the tomcat8 security including Java Keytool and connector for 8443.
#
# === Parameters
#
# [*port*]
#   The port for enabling SSL.
#   Default 8443
#
# [*org*]
#   The organization name used for the domain name of the keystore.
#   Default: 'tomcat'
#
# [*keystore_pass*]
#   The password for the keystore.
#
# [*keystore_file*]
#   The content of the java keystore to be used.
#   If left undef, this class will generate a self signed cert and keystore.
#   Default: undef
#
# === Variables
#
# [*keystore*]
#   The full path to the keystore.
#
# [*keytool*]
#   The full path to the keytool program.
#
# === Example
#
class tomcat8::security(
  $port                = '8443',
  $org                 = 'tomcat',
  $keystore_pass       = 'keystore',
  $keystore_file       = undef,
){

  anchor{'tomcat8::security::begin':
    require => Deploy_zip::Tar['tomcat8'],
  }

  # == variables == #
  $keystore    = "${tomcat8::tomcat_conf}/keystore.jks"
  $keytool     = '/usr/bin/keytool'

  if $keystore_file {
    $use_selfsigned = false
  }else{
    $use_selfsigned = true
  }

  if $use_selfsigned {
    # create the new keystore and self signed certificate
    exec {'create_keystore':
      command => "${keytool} -genkey -keyalg RSA -dname \"o=${org}\"\
 -alias tomcat -keypass ${keystore_pass} -keystore ${keystore} -storepass\
 ${keystore_pass} -validity 360 -keysize 2048",
      creates => $keystore,
      require => Anchor['tomcat8::security::begin'],
      before  => Concat::Fragment['server_ssl_connector'],
    }
  }else {
    file {$keystore:
      ensure  => file,
      content => $keystore_file,
      require => Anchor['tomcat8::security::begin'],
      before  => Concat::Fragment['server_ssl_connector'],
    }
  }

  # configure tomcat to add an additional connector!
  # add default connector
  concat::fragment{'server_ssl_connector':
    target  => $tomcat8::tomcat_server,
    content => "
  <Connector port=\"${port}\"
              protocol=\"org.apache.coyote.http11.Http11Protocol\"
              maxHttpHeaderSize=\"8192\"
              SSLEnabled=\"true\"
              maxThreads=\"150\"
              minSpareThreads=\"25\"
              enableLookups=\"false\"
              disableUploadTimeout=\"true\"
              acceptCount=\"100\"
              scheme=\"https\"
              secure=\"true\"
              clientAuth=\"false\"
              sslProtocol=\"TLS\"
              useBodyEncodingForURI=\"true\"
              keyAlias=\"tomcat\"
              keystoreFile=\"${keystore}\"
              keystorePass=\"${keystore_pass}\"
              keystoreType=\"JKS\"/>\n",
    order   => '03',
    notify  => Service['tomcat8'],
    require => Anchor['tomcat8::security::begin'],
  }

  anchor{'tomcat8::security::end':
    require => Concat::Fragment['server_ssl_connector'],
  }
}
